<?php
#Name:Divide Large Number v1
#Description:Divide a large number (such as those exceeding PHP max interger) by another large number, but don't calulator remainder (Eg: 3 not 3.3 or 3.67). Returns the number as a string on success. Returns "FALSE" on failure.
#Notes:Optional arguments can be "NULL" to skip them in which case they will use default values.
#Arguments:'number1' (required) is a string containing the number to divide. 'number2' (required) is a string containing the number to divide by. 'display_errors' (optional) indicates if errors should be displayed.
#Arguments (Script Friendly):number:string:required,display_errors:bool:optional
#Content:
if (function_exists('divide_large_number_v1') === FALSE){
function divide_large_number_v1($number1, $number2, $display_errors = NULL){
	$errors = array();
	$progress = '';
	##Arguments
	if ($number1 == '0'){
		#Do nothing to avoid ctype_digit thinking 0 === FALSE
	} else if (@ctype_digit($number1) === FALSE){
		$errors[] = "number1";
	}
	if ($number2 == '0'){
		#Do nothing to avoid ctype_digit thinking 0 === FALSE
	} else if (@ctype_digit($number2) === FALSE){
		$errors[] = "number2";
	}
	if ($display_errors === NULL){
		$display_errors = FALSE;
	}
	if ($display_errors === TRUE OR $display_errors === FALSE){
		#Do Nothing
	} else {
		$errors[] = "display_errors";
	}
	##Task [Decrement by divisor, and increment result by 1 when completed. Calculation is without remainder so if last section cannot be decremented by full divisor then don't increment result by 1. The result is NOT rounded to the nearest whole number.]
	if (@empty($errors) === TRUE){
		$result = '0';
		$location = realpath(dirname(__FILE__));
		$check = FALSE;
		do {
			$i = '0';
			do {
				require_once $location . '/dependencies/decrement_large_number_v1.php';
				$number1 = @decrement_large_number_v1($number1, FALSE);
				require_once $location . '/dependencies/increment_large_number_v1.php';
				$i = @increment_large_number_v1($i, FALSE);
				if ($number1 === '0'){
					$check = TRUE;
				}
			} while ($i != $number2 and $check === FALSE);
			if ($i === $number2){
				require_once $location . '/dependencies/increment_large_number_v1.php';
				$result = @increment_large_number_v1($result, FALSE);
			}
		} while ($number1 != '0');
	}
	result:
	##Display Errors
	if ($display_errors === TRUE and @empty($errors === FALSE)){
		$message = @implode(", ", $errors);
		if (function_exists('divide_large_number_v1_format_error') === FALSE){
			function divide_large_number_v1_format_error($errno, $errstr){
				echo $errstr;
			}
		}
		set_error_handler("divide_large_number_v1_format_error");
		trigger_error($message, E_USER_ERROR);
	}
	##Return
	if (@empty($errors) === TRUE){
		return $result;
	} else {
		return FALSE;
	}
}
}
?>